package com.cgdstnc.searchtask.repository;

import com.cgdstnc.searchtask.model.Product;
import com.cgdstnc.searchtask.model.dto.ProductTreeData;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jdk.nashorn.internal.runtime.regexp.joni.Regex;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Pattern;

/*
 * 27.06.2021
 * cagdastunca
 * */
@Configuration
public class MockDataRepository {

    private static HashMap<String, ProductTreeData> root;

    private ProductTreeData appendSentence(HashMap<String, ProductTreeData> node, String[] words, Product p, Integer level) {
        if (words == null || words.length == 0) return null;
        String firstWord = words[0];
        String[] rest = Arrays.copyOfRange(words, 1, words.length);

        ProductTreeData edge = node.getOrDefault(firstWord,
                ProductTreeData
                        .builder()
                        .word(firstWord)
                        .level(level)
                        .leaves(new HashMap<>())
                        .products(new ArrayList<>())
                        .build());

        ArrayList<Product> products = edge.getProducts();
        HashMap<String, ProductTreeData> leaves = edge.getLeaves();
        products.add(p);
        ProductTreeData children = appendSentence(leaves, rest, p, level + 1);
        if (rest.length > 0) {
            leaves.put(rest[0], children);
        }
        node.put(firstWord, edge);
        return edge;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void populateTree() {
        root = new HashMap<>();
        File data = new File("./data/sample_case5.json");
        try (InputStream in = new FileInputStream(data)) {
            ObjectMapper mapper = new ObjectMapper();
            ArrayList<Product> productList = mapper.readValue(in, new TypeReference<ArrayList<Product>>() {
            });
            if (productList != null) {
                for (Product p : productList) {
                    String title = p.getTitle();
                    String[] words = title.split(Pattern.quote(" "));
                    appendSentence(root, words, p, 0);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static HashSet<Product> search(HashMap<String, ProductTreeData> node, String query, Set<Product> list, boolean found) {
        for (Map.Entry<String, ProductTreeData> entry : node.entrySet()) {
            String word = entry.getKey();
            ProductTreeData data = entry.getValue();
            if (word.equalsIgnoreCase(query) && !found) {
                found = true;
                list.addAll(data.getProducts());
            }
            if (data.getLeaves().isEmpty()) {
                continue;
            }
            list.addAll(search(data.getLeaves(), query, list, found));
        }
        return new HashSet<Product>(list);
    }
    /*
    * # known issues & improvements
    *  - Search is not working as intended tree search needs improvement.
    *  - Category value should be added as weight for edges for sorting.
    * */
    public static HashSet<Product> search(String query) {
        HashSet<Product> results = new HashSet<>();
        if (query != null) {
            results.addAll(_search(query));
            String[] words = query.split(Pattern.quote(" "));
            if (words.length > 1) {
                for (String w : words) {
                    results.addAll(_search(w));
                }
            }
        }
        return results;
    }

    public static List<Product> _search(String query) {
        ArrayList<Product> unfilteredResults = new ArrayList<>(search(root, query, new HashSet<>(), false));
        ArrayList<Product> filteredResults = new ArrayList<>();
        if (unfilteredResults != null) {
            for (Product p : unfilteredResults) {
                if (p.getIsActive()) {
                    filteredResults.add(p);
                }
            }
        }
        filteredResults.sort(new Comparator<Product>() {
            @Override
            public int compare(Product o1, Product o2) {
                Integer category1 = o1.getCategory();
                Integer category2 = o2.getCategory();
                if (category1 == null || category2 == null) {
                    return 1;
                }
                return category1.compareTo(category2) * -1;
            }
        });
        return filteredResults;
    }

    public static HashMap<String, ProductTreeData> getRoot() {
        return root;
    }
}
