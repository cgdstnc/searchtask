package com.cgdstnc.searchtask.controller;

import com.cgdstnc.searchtask.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/*
 * 27.06.2021
 * cagdastunca
 * */
@Controller
@RequestMapping(value = "/")
public class HomeController {

    @Autowired
    private SearchService searchService;

    @GetMapping(value = "/")
    public String home() {
        return "redirect:/swagger-ui.html";
    }

    @ResponseBody
    @GetMapping(value = "/search")
    public ResponseEntity search(@RequestParam(name = "query") String query) {
    return searchService.search(query);
    }

    @ResponseBody
    @GetMapping(value = "/all")
    public ResponseEntity all() {
    return searchService.all();
    }



}