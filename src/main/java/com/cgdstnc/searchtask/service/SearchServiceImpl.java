package com.cgdstnc.searchtask.service;

import com.cgdstnc.searchtask.repository.MockDataRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/*
 * 28.06.2021
 * cagdastunca
 * */
@Service
public class SearchServiceImpl implements SearchService {

    @Override
    public ResponseEntity search(String query) {
        return ResponseEntity.ok(MockDataRepository.search(query));
    }

    @Override
    public ResponseEntity all() {
        return ResponseEntity.ok(MockDataRepository.getRoot());
    }
}
