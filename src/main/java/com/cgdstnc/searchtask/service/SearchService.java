package com.cgdstnc.searchtask.service;

import org.springframework.http.ResponseEntity;

/*
 * 28.06.2021
 * cagdastunca
 * */
public interface SearchService {

    ResponseEntity search(String query);

    ResponseEntity all();

}
