package com.cgdstnc.searchtask.model.dto;

import com.cgdstnc.searchtask.model.Product;
import lombok.*;

import java.util.ArrayList;
import java.util.HashMap;

/*
 * 27.06.2021
 * cagdastunca
 * */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ProductTreeData {

    private String word;
    private Integer level;
    private ArrayList<Product> products;
    private HashMap<String, ProductTreeData> leaves;
}
