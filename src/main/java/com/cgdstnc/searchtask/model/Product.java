package com.cgdstnc.searchtask.model;

import lombok.*;

/*
 * 27.06.2021
 * cagdastunca
 * */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Product {

    private String productId;
    private String title;
    private Integer category;
    private Boolean isActive;
}
