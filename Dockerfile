FROM openjdk:8-jre-alpine
ADD target/searchtask-0.0.1-SNAPSHOT.jar searchtask.jar
ADD data/sample_case5.json data/sample_case5.json
EXPOSE 8082
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom", "-jar","-Dspring.profiles.active=prod","-Dfile.encoding=UTF-8","-Duser.timezone=GMT+3", "searchtask.jar"]